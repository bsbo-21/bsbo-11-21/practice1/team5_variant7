﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SURVsystem
{
    class Class1
    {
        static void Main(string[] args)
        {

            DateTime changeTime = new DateTime(2023, 08, 14);
            SURVsystem surv = new SURVsystem(123, 14);


            Console.WriteLine("Контроль прихода и ухода персонала");
            Console.WriteLine("----------------------------------");
            surv.EnterCard(123);//Ввод личной карточки


            Console.WriteLine("Ввод задания");
            Console.WriteLine("------------------------");
            surv.EnterTasks(14); //Вводим номер задания 


            Console.WriteLine("Выбор задания работником");
            Console.WriteLine("---------------------------");
            surv.getAllTasks();//Выводим все задания
            surv.getTask(15);//запрашиваем задание


            Console.Read();
        }
    }


    public class SURVsystem
    {
        private int _person;
        private int _taskId;

        AccessControl accessControl = new AccessControl(123, 0);
        TaskList taskList = new TaskList(14, new List<int> { });

        public List<int> activeTasks = new List<int> { };

        public int Person { get { return _person; } set { _person = value; } }

        public int TaskId { get { return _taskId; } set { _taskId = value; } }

        /*This is Выбор задания работником*/
        
        public int getAllTasks()
        {
            Console.WriteLine("SURVsystem.getAllTasks   \n");
            taskList.getAllTasks(taskList.tasks);
            

            return this.Person;
        }

        public int getTask(int taskId)
        {
            Console.WriteLine("SURVsystem.getTask \n");
            activeTasks.Add(taskId);
            taskList.DeleteTask(taskId);
            this.TaskId = taskId;
            return TaskId;


        }

        /*This is Ввод задания*/

        public int EnterTasks(int TaskId)
        {
            Console.WriteLine("SURVsystem.enterTask   \n");
            this.TaskId = TaskId;
            taskList.EnterTask(TaskId);
            taskList.getAllTasks(taskList.tasks);
            return this.TaskId;

        }


        /*This is Контроль прихода и ухода персонала*/

        public int EnterCard(int Person)
        {
            Console.WriteLine("SURVsystem.enterCard   \n");
            this.Person = Person;

            accessControl.RecordVisit(Person);

            if (Person == 123)//у нас может быть только 2 работника
            {
                accessControl.worker1.PersonalVisits++;
                return accessControl.worker1.PersonalVisits;
            }
            else
            {
                accessControl.worker2.PersonalVisits++;
                return accessControl.worker2.PersonalVisits;
            }
        }
        public SURVsystem(int person, int taskId)
        {
            Person = person;
            TaskId = taskId;
        }

    }

    public class AccessControl
    {
        private int _personalId;
        private int _quantity;
        private int _personalVisits;

        public int PersonalId { get { return _personalId; } set { _personalId = value; } }
        public int Quantity { get { return _quantity; } set { _quantity = value; } }

        public int PersonalVisits { get { return _personalVisits; } set { _personalVisits = value; } }

        public Worker worker1 = new Worker(123, 0);//новичок (0 посещений)
        public Worker worker2 = new Worker(124, 3);//опытный (3 посещения)

        public int RecordVisit(int personalId)
        {
            Console.WriteLine("AccessControl.RecordVisit \n");
            if (personalId==123)
            {
                worker1.PersonalVisits++;
                return worker1.PersonalVisits;
            }
            else
            {
                worker2.PersonalVisits++;
                return worker1.PersonalVisits;
            }
        }


        public AccessControl(int personalId, int personalVisits)
        {

            PersonalId = personalId;
            PersonalVisits = personalVisits;

        }
    }

    public class TaskList
    {
        private int _taskId;


        public int num = 14;
        public List<int> tasks = new List<int> { };

        public int TaskId { get { return _taskId; } set { _taskId = value; } }


        public void EnterTask(int TaskId)
        {
            tasks.Add(TaskId);
            Console.WriteLine("TaskList.enterTask   \n");
            this.TaskId = _taskId;

        }

        public int DeleteTask(int TaskList)
        {
            tasks.Remove(TaskId);
            Console.WriteLine("TaskList.DeleteTask   \n");
            this.TaskId = 0;
            return this.TaskId;
        }
        public void getAllTasks(List<int> tasks)
        {
            Console.WriteLine("TaskList.getAllTasks \n");
            foreach (int task in tasks)
            {
                // Console.WriteLine(task); //вывод тасков
            }
        }

        public TaskList(int taskId, List<int> values)

        {

            TaskId = taskId;
            tasks = values;

        }
    }



    public class Worker
    {


        private int personalId;

        public List<int> activeTasks = new List<int>();

        public int PersonalVisits;
        public int PersonalId { get { return personalId; } set { personalId = value; } }


       

        public Worker( int personalId, int personalVisits)
        {
            
            PersonalId = personalId;
            PersonalVisits = personalVisits;
        }
    }


}
