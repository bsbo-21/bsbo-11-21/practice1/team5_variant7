#Выбор заданий

```plantuml 
@startuml

actor Worker

activate SURVSystem
activate TaskList


Worker -> SURVSystem: getAllTasks()
SURVSystem -> TaskList: getAllTasks()
TaskList -> SURVSystem: allTasks(tasks)
SURVSystem -> Worker: Доступные задания

Worker -> SURVSystem: getTask(taskId)
SURVSystem -> TaskList: getTask(taskId)
TaskList -> SURVSystem: task
SURVSystem -> TaskList: deleteTask(taskId)
SURVSystem -> Worker: Задание


@enduml
```
