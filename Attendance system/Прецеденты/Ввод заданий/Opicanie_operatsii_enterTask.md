

Руководитель, авторизовавшись в СУРВ-системе, вводит номер задачи и количество задач на выполнение, что позволяет облешчить распределение задач.

Операция: enterTask(TaskID: TaskID, quantity: integer)

Ссылки: Прецеденты: Отслеживание хода работы

Предусловия: Инициирована задача

Постусловия:

- Создан экземпляр класса Tasklist (создание экземпляра)
- Экземпляр связан с текущим экземпляром класса Computer (формирование ассоциации)
- Задача попадает в СУРВ--систему, откуда выдается исполнителю
