#отслеживание хода работы

```plantuml 
@startuml

actor Руководитель

Руководитель -> SURVSystem: enterTask(Taskid, Quantity)

activate TaskList
activate SURVSystem
SURVSystem -> TaskList: enterTask(TaskId, Quantity)
TaskList -> SURVSystem: getAllTasks(tasks)
SURVSystem -> Руководитель: Все задания данного типа


@enduml
```
